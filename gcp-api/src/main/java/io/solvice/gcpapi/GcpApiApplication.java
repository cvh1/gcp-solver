package io.solvice.gcpapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import lombok.*;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gcp.pubsub.PubSubAdmin;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.cloud.gcp.storage.GoogleStorageResource;
import org.springframework.stereotype.Service;
import org.springframework.util.SerializationUtils;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Objects;

@SpringBootApplication
public class GcpApiApplication implements CommandLineRunner {

    public GcpApiApplication(PubSubService pubsub) {
        this.pubsub = pubsub;
    }

    public static void main(String[] args) {
		SpringApplication.run(GcpApiApplication.class, args);
	}


    private final PubSubService pubsub;
    @Override
    public void run(String... args) throws Exception {
//        pubsub.subscribe();
        pubsub.receiveJob();
    }
}


@Service
@AllArgsConstructor
@Log4j2
class PubSubService {

	private final PubSubTemplate pubSubTemplate;
	private final PubSubAdmin pubSubAdmin;
	private final String Q = "solverQ";
	private final String SUB = "sub";

	private final Solver solver;

	public void receiveJob() {
		pubSubTemplate.subscribe(SUB, (message, consumer) -> {
            log.info("Message received from " + Q + " subscription. " + message.getMessageId());
            consumer.ack();

            String job = (String) SerializationUtils.deserialize(message.getData().toByteArray());
            solver.execute(Objects.requireNonNull(job));
        });
	}

	public void subscribe() {
    this.pubSubAdmin.createSubscription(SUB, Q);

    }

}


@Data
@AllArgsConstructor
@NoArgsConstructor
class SolveJob {
    private String id;
    private String status;
}


@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
class Model extends SolveJob implements Serializable {

    List<String> locations;

    @Override
    public String toString() {
        return "Model{" +
                "id=" + getId() +
                '}';
    }
}


@AllArgsConstructor
@Service
@Slf4j
class JobStorage {

    private final Storage storage;

    private final String gcsLocation = "gs://solve-jobs/";


    public Model readJob(String id) throws Exception {
        final GoogleStorageResource gcsFile = new GoogleStorageResource(storage, gcsLocation + id);
        ObjectMapper mapper = new ObjectMapper();
        Model model = mapper.readValue(gcsFile.getInputStream(), Model.class);
        log.info("read model {}",model.toString());
        return model;
    }



}


@Service
@Slf4j
class Solver {

    private final JobStorage storage;

    Solver(JobStorage storage) {
        this.storage = storage;
    }

    public void execute(String job){
        try {
            log.info("start solver");

            storage.readJob(job);
        } catch (Exception e) {
            log.error(e.toString());
        }
    }
}