package io.solvice.gcpsolver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.common.collect.Lists;
import com.google.protobuf.ByteString;
import com.google.pubsub.v1.PubsubMessage;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.SerializationUtils;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gcp.pubsub.PubSubAdmin;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.cloud.gcp.storage.GoogleStorageResource;
import org.springframework.core.io.WritableResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;
import org.springframework.util.concurrent.ListenableFuture;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;

@SpringBootApplication
@Slf4j
public class GcpSolverApplication implements CommandLineRunner {

    public GcpSolverApplication(PubSubService pubsub, JobStorage storage) {
        this.pubsub = pubsub;
        this.storage = storage;
    }

    public static void main(String[] args) {
		SpringApplication.run(GcpSolverApplication.class, args);
	}

	private final PubSubService pubsub;
    private final JobStorage storage;


    private void process(Model model) throws Exception {
        storage.writeGcs(model);
        pubsub.sendJob(model);

    }
	@Override
	public void run(String... args) throws Exception {
        for (int i = 0; i < 100; i++) {
            process(new Model("id"+i));
        }
	}
}

@Service
@AllArgsConstructor
class PubSubService {

	private final PubSubTemplate pubSubTemplate;
	private final PubSubAdmin pubSubAdmin;
	private static final String Q = "solverQ";

	public String sendJob(SolveJob job) throws ExecutionException, InterruptedException {
	    PubsubMessage msg = PubsubMessage.newBuilder()
                .setMessageId(job.getId())
                .setData(ByteString.copyFrom(SerializationUtils.serialize(job.getId())))
                .build();
        ListenableFuture<String> publish = pubSubTemplate.publish(Q, msg);
        return publish.get();
    }

	public void create() {
	    pubSubAdmin.createTopic(Q);
    }

}

@Data
@AllArgsConstructor
@NoArgsConstructor
class SolveJob  implements Serializable {
	private String id;
	private String status;
}

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class Model extends SolveJob implements Serializable {

    public Model(String id) {
        setId(id);
    }

    List<String> locations;

}


@AllArgsConstructor
@Service
@Slf4j
class JobStorage {
    private final Storage storage;
    private final String gcsLocation = "solve-jobs";


    public void writeGcs(Model data) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Blob blob = storage.create(BlobInfo.newBuilder(gcsLocation, data.getId()).build(),
                mapper.writeValueAsBytes(data));
        log.info("write object "+data.getId());
    }

}
